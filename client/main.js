import React from 'react';
import { Meteor } from 'meteor/meteor';
import ReactDOM from 'react-dom';
import appUI from '../imports/ui/appUI.js';
const rcE = React.createElement;//shorthand

Meteor.startup(function(){
  /*if user's logged in already*/
  if(Meteor.userId())
  {
    Accounts.onLogin(() => {
      /*check for revocation of GDrive access*/
      Meteor.call('checkRevocation', Meteor.userId(),(err, res) => {
        if (err) {
          Meteor.logout( ()=>{
            ReactDOM.render(rcE(appUI, {
              ShowSplash:false,
              ShowAuth:true,
              ShowDlg:true,
              dlgInfo:{title:"Notification", text:err.reason},
              dlgBtns:rcE("button",{key:"dialog-hide", "data-dismiss":"modal", className:"btn btn-success btn-block"
            },"Ok")}), document.getElementById('root'));
          } );
        } else {
          ReactDOM.render(rcE(appUI, {ShowSplash:false, ShowAuth:false}), document.getElementById('root'));
        }
      });
    });
  }
  else
  {
    ReactDOM.render(rcE(appUI, {ShowSplash:false, ShowAuth:true}), document.getElementById('root'));
  }
});
