import { HTTP } from 'meteor/http';
import { Meteor } from 'meteor/meteor';
import { ServiceConfiguration } from 'meteor/service-configuration';
import { Random } from 'meteor/random';
import { ValidationError } from 'meteor/jagi:astronomy';
import RandToken from 'rand-token';
import md5 from 'md5';
import { timePassed, fileFuncsCheck, makeError } from "/imports/api/helperFuncs.js";

/*collections wont be available via http*/
SimpleRest.configure({
  collections: []
});
import {File, fileList, clientsFiles, clients, client} from "/imports/api/collections.js";
/*
method ensures no one can pass a fake UserId and disable someone's else client
extending it only on server, so clients won't be able to mess with it
*/
client.extend({
  meteorMethods: {
    belongsTo(userId) {
      return this.userId === userId;
    }
  }
});

Accounts.onCreateUser((options, user) => {
  // Generate a user ID ourselves since it hasnt been created yet
  user._id = Random.id();
  user.username = user.services.google.name;
  /*also initializing am empty clients array*/
  user.clients = [];
  return user;
});

ServiceConfiguration.configurations.upsert(
  { service: "google" },
  { $set: { clientId: Meteor.settings.private.google.clientId, secret: Meteor.settings.private.google.secret } }
);

Meteor.publish("clientUpdate", function (clientId) {
  return client.find({id: clientId},{fields:{id:1,active: 1}});
}, {
  url: "clientUpdate/:0",
  httpMethod: "get"
});

/*method allows subscrption for a single client by the provided id or by id and access_token in case it was an http request*/
Meteor.publish('clients.one', function(clientId) {
  const fields_option = {
    id: 1,
    name: 1,
    pathToDir:1,
    active: 1
  };
  if (!this.userId) {
    return this.ready();
  }
  return client.find({id: clientId}, {fields: fields_option});
});

/*method allows subscrption for a single file collection by the provided client id*/
Meteor.publish('fileList.one', function(clientId) {
  if (!this.userId) {
    return this.ready();
  }

  return fileList.find({client_id: clientId});
});

Meteor.publish('clients.all', function() {
  if (!this.userId) {
    return this.ready();
  }
  return client.find({
    userId: this.userId
  }, {
    fields: {
      userId:1,
      id: 1,
      name: 1,
      encrData: 1,
      pathToDir:1,
      active: 1
    }
  });
});


JsonRoutes.add("get", "/authStart", function (req, res, next) {
  const {response_type, client_id, redirect_uri} = req.query;
  var auth_client = client.findOne({id:client_id});
  //default response is for 404 error
  var result = {
    code:404,
    headers:{"Reason-Phrase":"Bad query parameters"}
  };
  if(response_type==="code" && auth_client)
  {
    if(auth_client.active)
    {
      let time_passed = timePassed(auth_client.auth_code.lastRequested);
      if(time_passed > 60)
      {
        /*a fallback for resolving several possible consequent activations of a single client*/
        if(auth_client.access_token)
        {
          result = {
            code:401,
            headers:{"Reason-Phrase":"Second authorization of the client cannot be performed. You can deactivate this client in the control panel"}
          };
        }
        else {
          /*md5 sum ensures it's not possible to substitute client code or auth code*/
          let authCode = Random.id(8), clCode = [`${new Date().getTime()}`.slice(-3), Math.floor(Math.random()*899+100)].join("");
          auth_client.set({clCode:{
            code:clCode,
            md5:md5(authCode+clCode)
          }});
          auth_client.save();
          result = {
            /*redirecting user further*/
            code:302,
            headers:{Location: `${redirect_uri}/${clCode}?authCode=${authCode}&client_id=${client_id}`}
          };
        }
      }
      else
      {
        result = {code:429, headers:{"Retry-After":60 - time_passed}}
      }
    }
    else {
      result = {
        code:403,
        headers:{"Reason-Phrase":"This client needs to be actived in order to be able to receive an authorization code. User can activate this client in the control panel"}
      };
    }
  }
  JsonRoutes.sendResult(res, result);
});

JsonRoutes.add("get", "/authEnd/:URLclCode", function (req, res, next) {
  const {authCode, client_id} = req.query;
  const URLclCode = req.params.URLclCode;
  let auth_client = client.findOne({id:client_id, "clCode.code":URLclCode});
  //check clCode
  if(auth_client && auth_client.clCode.md5 === md5(authCode+URLclCode))
  {
    auth_client.set({
      auth_code:{code:authCode,lastRequested:new Date().getTime()},
      clCode:{code:"", md5:""}
    });
    auth_client.save();
    JsonRoutes.sendResult(res, {
      data: {authCode, expireTimeSecs:180}
    });
  }
  else {
    JsonRoutes.sendResult(res, {
      code:404,
      headers:{Warning:"Parameters mismatch"}
    });
  }
});

/*
method allows user to download app archive from server with necessary data zipped inside
currently is not used
*/
JsonRoutes.add("get", "/:dotUserId/:client_id", function (req, res, next) {
  let {dotUserId, client_id} = req.params;
  let fs = Npm.require('fs');
  try {
    let path = process.cwd().split("\\").join("/")+`/${dotUserId}/${client_id}`;
    let stat = fs.statSync(path);
    let data_read = fs.readFileSync(path);
    //need to manually write headers and set the message body since sendResult method sets response body encoding to application/json
    res.writeHead(200, {'Content-type': 'application/zip', 'Content-Transfer-Encoding':'binary' ,'Content-Length': stat.size,'Content-Disposition': `attachment; filename=${client_id}`});
    res.write(data_read);
  } catch(err) {
    res.writeHead(301, {Location:"/"});
  }
  JsonRoutes.sendResult(res);
});

SimpleRest.setMethodOptions('googleUserTokens', {
  url: "googleUserTokens",
  getArgsFromRequest: function (request) {
    var content = request.body;
    return [ content.access_token, content.client_id ];
  }
});

SimpleRest.setMethodOptions('token', {
  url: "token",
  getArgsFromRequest: function (request) {
    var content = request.body;
    return [ content.grant_type, content.client_id, content.refresh_token, content.secret, content.code ];
  }
});

SimpleRest.setMethodOptions('revokeAccess', {
  url: "revokeAccess",
  getArgsFromRequest: function (request) {
    var content = request.body;
    return [ content.client_id, content.token ];
  }
});

SimpleRest.setMethodOptions('clientFilesHTTPAdd', {
  url: "clientFilesHTTPAdd",
  getArgsFromRequest: function (request) {
    var content = request.body;
    return [ content.client_id, content.access_token, content.JSON_data, content.path ];
  },
  httpMethod:"post"
});

SimpleRest.setMethodOptions('clientFilesHTTPUpdate', {
  url: "clientFilesHTTPUpdate",
  getArgsFromRequest: function (request) {
    var content = request.body;
    return [ content.client_id, content.access_token, content.JSON_data, content.path ];
  },
  httpMethod:"patch"
});

SimpleRest.setMethodOptions('clientFilesConstUpdate', {
  url: "clientFilesConstUpdate",
  getArgsFromRequest: function (request) {
    var content = request.body;
    return [ content.client_id, content.access_token, content.JSON_data, content.path ];
  },
  httpMethod:"patch"
});

SimpleRest.setMethodOptions('clientFilesHTTPDelete', {
  url: "clientFilesHTTPDelete",
  getArgsFromRequest: function (request) {
    var content = request.body;
    return [ content.client_id, content.access_token, content.JSON_data ];
  },
  httpMethod:"delete"
});

/*proper handling of Meteor Error sent back via http*/
JsonRoutes.ErrorMiddleware.use(RestMiddleware.handleErrorAsJson);

Meteor.methods({
  /*revoking access from client app (access token will be removed in any case, refresh token will remain intact only if method was called as a server method i.e. user deactivated client)*/
  'revokeAccess'(client_id, token, user_id){
    //searching either using a id+token or id+userId pair. The latter one is used when method was call on the server, the former one is for API call
    var req_client = client.findOne({id:client_id, $or:[{access_token:token},{refresh_token:token},{userId:user_id}]});
    try {
      req_client.set({
        access_token:"",
        //if token is presented, method was called as API, which requires full revocation including refresh_token
        refresh_token: token ? "" : req_client.refresh_token,
        expires_at:0
      });
    } catch (e) {
      makeError("revokeAccess.notFound", "Couldn't find a client with specified params", 404);
    }
    req_client.validate(function(err){
      if(err)
      {
        if (ValidationError.is(err)) {
          makeError('revokeAccess.incorrect', 'Validation failed', 409);
        }
          makeError('revokeAccess.unknown', "Something went wrong", 400);
      }
    });
    req_client.save();
  },
  /*method for activating and deactivating clients*/
  'clients.changeActState'({ client_id, newState }) {
    if (!this.userId) {
      makeError('clients.changeActState.incorrect','User unauthorized');
    }
    if(!newState)
      Meteor.call('revokeAccess', client_id,"",this.userId);
    var newClient = client.findOne({userId:this.userId, id:client_id});
    newClient.active = newState;
    newClient.validate(function(err){
      if(err)
      {
        if (ValidationError.is(err)) {
          makeError('clients.changeActState.incorrect', 'Validation failed');
        }
          makeError('clients.changeActState.unknown', "Something went wrong");
      }
    });

    if(newClient.belongsTo(this.userId))
    {
        newClient.save();
        return newClient.active;
    }
  },
  /*method softly removes (i.e. marks as deleted) a client from the database making it no longer available for usage*/
  'clients.deleteClient'(clientId){
    if (!this.userId) {
      makeError('clients.deleteClient.incorrect', 'User unauthorized');
    }
    var reqClient = client.findOne({id:clientId});
    var fl = fileList.findOne({client_id:reqClient.id});
    if(reqClient && reqClient.belongsTo(this.userId))
    {
        reqClient.softRemove();
        if(fl)
          fl.remove();
        Meteor.users.update({_id:this.userId},{$pull:{clients:reqClient.id}});
        return true;
    }
    /*throw an error in case client doesnt actually belong to the user, not stating the exact error for security reasons*/
    makeError('clients.deleteClient.incorrect', 'Inappropriate data passed');
  },
  /*method for registering a new client app*/
  'clients.addNew'(){
    if (!this.userId) {
      throw new Meteor.Error('clients.addNew.incorrect',
        'User unauthorized');
    }
    const user = Meteor.users.findOne({_id:this.userId}),
    client_id = Random.id(10),
    client_secret = Random.secret(),
    root_url = process.env.ROOT_URL,
    orig = JSON.stringify({client_id, client_secret, root_url, endpoint_path:`/authStart?response_type=code&client_id=${client_id}&redirect_uri=${root_url}/authEnd`, googleClientId: Meteor.settings.private.google.clientId, googleSecret: Meteor.settings.private.google.secret}),
    encrypted = CryptoJS.AES.encrypt(orig, Meteor.settings.private.passphrase);
    var newClient = new client({
      userId:this.userId,
      id:client_id,
      secret:client_secret,
      encrData:encrypted.toString(),
      name:`New client ${user.clients.length+1}`,
      active:false
    });
    newClient.validate(function(err) {
      if(err)
      {
        if (ValidationError.is(err)) {
          makeError('clients.testAddClient.incorrect', 'Validation failed');
        }
        makeError('clients.testAddClient.unknown', "Something went wrong");
      }
    });
    newClient.save();
    Meteor.users.update({_id:this.userId},{$push:{clients:newClient.id}});
    return newClient.id;
  },
  /*method allows to create a downloadable zip archive of a client app with necessary data inside*/
  'makeClientZip'(client_id){
    if (!this.userId) {
      throw new Meteor.Error('clients.addNew.incorrect',
        'User unauthorized');
    }
    const reqClient = client.findOne({id:client_id, userId:this.userId});
    if(!reqClient || !reqClient.belongsTo(this.userId))
    {
      makeError('makeClientZip.incorrect', 'Inappropriate data passed');
    }
    var JSZip = Npm.require("jszip");
    var zip = new JSZip();
    var fs = Npm.require('fs');
    //fs seems to work badly with strings that contain backslashes
    const path = process.cwd().split("\\").join("/")+`/.${this.userId}`;
    //final path of the client zip archive
    const final = `${path}/${client_id}.zip`;
    if(fs.existsSync(final))
      return final;
    try {
      if (!fs.existsSync(path))
        fs.mkdirSync(path);
    } catch (e) {
      makeError("clients.addNew.appGenerate", "Couldn't generate an application archive");
    }
    fs.readFile(Assets.absoluteFilePath("zippy.zip"), function (err, data) {
      if (err)
        makeError("clients.addNew.appGenerate", "Couldn't generate an application archive");
      zip.loadAsync(data).then(function(zip){
        const root_url = process.env.ROOT_URL || "localhost:3000/";
        //need to cypher this data
        zip.file("data", JSON.stringify({client_id, client_secret:reqClient.secret, root_url, endpoint_path:`/authStart?response_type=code&client_id=${client_id}&redirect_uri=${root_url}authEnd`}));
        zip.generateNodeStream({streamFiles:true})
        .pipe(fs.createWriteStream(final))
        .on('finish', function () {
            // JSZip generates a readable stream with a "end" event,
            // but is piped here in a writable stream which emits a "finish" event.
            return final;
        });
      });
    });
  },
  //clears the log
  'clients.clearLog'(clientId)
  {
    if (!this.userId) {
      makeError('clients.clearLog.incorrect', 'User unauthorized');
    }
    var reqClient = client.findOne({id:clientId});
    var fl = fileList.findOne({client_id:reqClient.id});
    if(fl && reqClient.belongsTo(this.userId))
    {
        fl.filesLog = [];
        fl.save();
        return true;
    }
    /*throw an error in case client doesnt actually belong to the user, not stating the exact error for security reasons*/
    makeError('clients.clearLog.incorrect', 'Inappropriate data passed');
  },
  /*this is like a test method which generates files for clients*/
  'clientsFiles.testAddFilest'(clientId){
    let fArr = [];
    for(let i=0;i<20;i++)
    {
      fArr.push(new File({
        type: Math.round(Math.random()) ? "file" : "directory",
        name:'abcdefghijklmnopqrstuvwxyz'.split('').sort(()=>{return 0.5-Math.random()}).join(''),
        stat: ""+Math.floor(Math.random()*100)
      }));
    }
    let newTestfileList = fileList.findOne({client_id:clientId});
    if(!newTestfileList)
      newTestfileList = new fileList({
        client_id:clientId,
        files:fArr
      });
    else {
      newTestfileList.set({
        files:fArr
      });
    }
    newTestfileList.validate(function(err) {
      if(err)
      {
        if (ValidationError.is(err)) {
          makeError('clientsFiles.testAddFilest.incorrect',
            'Validation failed');
        }
        makeError('clientsFiles.testAddFilest.unknown',
          "Something went wrong");
      }
    });
    newTestfileList.save();
    return newTestfileList.id;
  },
  /*method check if user has revoken the access to their google drive from the application*/
  'checkRevocation'(userId){
    /*gonna check if user's access token has expired or been revoked*/
    const user = Meteor.users.findOne({_id:userId});
    try {
      let test = HTTP.get("https://www.googleapis.com/drive/v2/files", {
        params: {
          access_token: user.services.google.accessToken,
        }
      });
      return test
    } catch (e) {
      //access token has expired or access was revoked
        if(e.response && (e.response.statusCode === 401))
        {
          try {
            let result = HTTP.post("https://www.googleapis.com/oauth2/v4/token", {
              params: {
                refresh_token: user.services.google.refreshToken,
                client_id: Meteor.settings.private.google.clientId,
                client_secret:Meteor.settings.private.google.secret,
                grant_type:"refresh_token"
              }
            });
            Meteor.users.update({_id:userId},{$set:{"services.google.accessToken":result.data.access_token, "services.google.expiresAt":new Date().getTime()+result.data.expires_in*1000}});
            return result.data.token_type
          } catch (e) {
            //forcing user to login again
            Meteor.users.update({_id:userId}, {$set: { "services.resume.loginTokens" : [] }});
            makeError('checkRevocation.failed',
              "Access was revoked");
          }
        }
      }
  },
  /*method returns user's google access and refresh tokens and updates them if necessary*/
  'googleUserTokens'(access_token){
    /*using ublock to allow other methods execution in case we will have to make an HTTP call, which is going to be synchronous*/
    this.unblock();
    const reqClient = client.findOne({access_token});
    const reqClientId = reqClient ? reqClient.id : "";
    const user = Meteor.users.findOne({clients:reqClientId});
    if(user)
    {
      var result = {
        data:{accessToken:user.services.google.accessToken,expiresAt: user.services.google.expiresAt}
      };
      if(user.services.google.expiresAt < new Date().getTime())
      {
        /*google access token needs a refresh*/
        try
        {
          result = HTTP.post("https://www.googleapis.com/oauth2/v4/token", {
            params: {
              refresh_token: user.services.google.refreshToken,
              client_id: Meteor.settings.private.google.clientId,
              client_secret:Meteor.settings.private.google.secret,
              grant_type:"refresh_token"
            }
          });
          /*since google returns token lifetime instead of expiration time in a timestamp form, it needs to be converted manually*/
          result.data.expiresAt = new Date().getTime()+result.data.expires_in*1000;
          console.log(`at: ${result.data.expiresAt}; in: ${result.data.expires_in}`);
          Meteor.users.update({_id:user._id},{$set:{"services.google.accessToken":result.data.access_token, "services.google.expiresAt":result.data.expiresAt}});
        }
        /*client wasnt found*/
        catch (e)
        {
          //basically redirecting given error from Google to client
          makeError('googleUserTokens.refresh',e.response.data.error_description,e.response.statusCode);
        }
      }
      /*if everyting's ok, return the resulting object, which could have been updated or not */
      return result.data
    }
    makeError('googleUserTokens.incorrect', "Client wasn't found", 404);
  },
  /*creates either access and refresh tokens or renews an existing acess token with passed refresh*/
  'token'(grant_type, client_id, refresh_token, secret, code){
    /*unblock is required in order to allow other methods execution*/
    this.unblock();
    if(!grant_type || !client_id || !secret)
      makeError('token.params','Missing params', 401);
    switch (grant_type) {
      case "authorization_code":
      {
        let auth_client = client.findOne({id:client_id, secret, "auth_code.code":code});
        if(!auth_client)
          makeError('token.notFound',"Couldn't find a client with specified params", 404);
        const time_passed = timePassed(auth_client.auth_code.lastRequested);
        if(time_passed > 180)
          makeError('token.authExpired',`Authorization code ${code} has expired`,401);
        auth_client.set({
          access_token:RandToken.generate(54),
          /*seconds were converted to milliseconds*/
          expires_at:new Date().getTime()+86400000,
          refresh_token:RandToken.generate(46),
          auth_code:{}
        });
        auth_client.save();
        /*gotta use a method for retrieving google tokens*/
        const googleUserTokens = Meteor.call('googleUserTokens', auth_client.access_token);
        return {
          /*google refreshToken, accessToken, expiresAt*/
          access_token:auth_client.access_token,
          token_type:"bearer",
          expires_at:auth_client.expires_at,
          refresh_token:auth_client.refresh_token,
          googleUserTokens
        }
        break;
      }
      case "refresh_token":
      {
        let auth_client = client.findOne({id:client_id, refresh_token});
        if(!auth_client)
          makeError('token.notFound',"Couldn't find a client with specified params",404);
        if(!auth_client.active)
        {
          makeError('token.deactivated',"This client is currently deactivated",401);
        }
        auth_client.set({
          access_token:RandToken.generate(54),
          expires_at:new Date().getTime()+86400000,
          auth_code:{}
        });
        auth_client.save();
        return {
          access_token:auth_client.access_token,
          token_type:"bearer",
          expires_at:auth_client.expires_at
        }
        break;
      }
      default:
        makeError('token.incorrect',`Invalid grant_type ${grant_type} was passed`, 404);
      break;
    }
  },
  'clientFilesHTTPAdd'(clientId, acc_token, flLstJSON, pathToDir){
    const updClt = client.findOne({access_token:acc_token, id:clientId});
    const filesParsed = fileFuncsCheck(updClt, flLstJSON, "clientFilesHTTPAdd");
    let filesDB = fileList.findOne({client_id:clientId});
    const arrOfFiles = Array.isArray(filesParsed) ? filesParsed : [filesParsed];
    if(!filesDB || !filesDB.files)
    {
      //filelist doesnt exist in the database
      filesDB = new fileList({
        client_id:clientId,
        pathToDir:fileList.pathToDir || "",
        files: arrOfFiles
      });
    }
    else {
      arrOfFiles.forEach((httpFile)=>{
        //found the same file in database
        if(filesDB.files.findIndex((dbFile) => {return (httpFile.name === dbFile.name) && (httpFile.type === dbFile.type)}) != -1)
          makeError('clientFilesHTTPAdd.filesExists',"One or several files already exist",409);
      });
      //adding to file list
      filesDB.files.push.apply(filesDB.files, arrOfFiles);
    }
    if(pathToDir && !filesDB.pathToDir)
      filesDB.pathToDir = pathToDir;
    filesDB.validate(function(err) {
      if(err)
      {
        if (ValidationError.is(err)) {
          makeError('clientFilesHTTPAdd.inappropData', "Data passed couldn't be validated", 404);
        }
        makeError(`clientFilesHTTPAdd.${err.error}`, "An error occured. Try requesting later", 404);
      }
    });
    filesDB.save();
    return arrOfFiles.map((file)=>{return file.name});
  },
  //this one moves files to log
  'clientFilesHTTPUpdate'(clientId, acc_token, flLstJSON){
    const updClt = client.findOne({access_token:acc_token, id:clientId});
    const clFiles = fileFuncsCheck(updClt, flLstJSON, "clientFilesHTTPUpdate");
    let filesDB = fileList.findOne({client_id:clientId});
    const arrOfFiles = Array.isArray(clFiles) ? clFiles : [clFiles];
    if(!filesDB || !filesDB.files || filesDB.files.length == 0)
      makeError('clientFilesHTTPUpdate.notFound', "Nothing to update", 404);
    arrOfFiles.forEach((httpFile)=>{
      if(!httpFile.name || !httpFile.type)
        makeError('clientFilesHTTPUpdate.empty', "No data provided", 204);
      let index = filesDB.files.findIndex((dbFile) => {return (httpFile.name === dbFile.name) && (httpFile.type === dbFile.type)});
      if(index == -1)
        makeError('clientFilesHTTPUpdate.noFile',`File ${httpFile.name} doesn't exist`,404);
      filesDB.files[index].stat = httpFile.stat ? httpFile.stat : `${filesDB.files[index].stat} finished`;
      filesDB.filesLog.push(filesDB.files[index]);
      filesDB.files.splice(index,1);
    });
    filesDB.validate(function(err) {
      if(err)
      {
        if (ValidationError.is(err)) {
          makeError('clientFilesHTTPUpdate.inappropData', "Data passed couldn't be validated", 404);
        }
        makeError(`clientFilesHTTPUpdate.${err.error}`, "An error occured. Try requesting later", 404);
      }
    });
    filesDB.save();
    return arrOfFiles.map((file)=>{return file.name});
  },
  //this one updates file's status
  'clientFilesConstUpdate'(clientId, acc_token, flLstJSON, pathToDir){
    const updClt = client.findOne({access_token:acc_token, id:clientId});
    const clFiles = fileFuncsCheck(updClt, flLstJSON, "clientFilesHTTPUpdate");
    let filesDB = fileList.findOne({client_id:clientId});
    const arrOfFiles = Array.isArray(clFiles) ? clFiles : [clFiles];
    if(!filesDB || !filesDB.files || filesDB.files.length == 0)
      makeError('clientFilesHTTPUpdate.notFound', "Nothing to update", 404);
    arrOfFiles.forEach((httpFile)=>{
      if(!httpFile.name || !httpFile.type || !httpFile.updInfo)
        makeError('clientFilesHTTPUpdate.empty', "No data provided", 204);
      let index = filesDB.files.findIndex((dbFile) => {return (httpFile.name === dbFile.name) && (httpFile.type === dbFile.type)});
      if(index == -1)
        makeError('clientFilesHTTPUpdate.noFile',`File ${httpFile.name} doesn't exist`,404);
      filesDB.files[index].stat = httpFile.updInfo.stat || filesDB.files[i].stat;
      filesDB.files[index].name = httpFile.updInfo.name || filesDB.files[i].name;
      if(httpFile.updInfo.fileIcon)
        filesDB.files[index].fileIcon = httpFile.updInfo.fileIcon;
    });
    if(pathToDir)
      filesDB.pathToDir = pathToDir;
    filesDB.validate(function(err) {
      if(err)
      {
        if (ValidationError.is(err)) {
          makeError('clientFilesHTTPUpdate.inappropData', "Data passed couldn't be validated", 404);
        }
        makeError(`clientFilesHTTPUpdate.${err.error}`, "An error occured. Try requesting later", 404);
      }
    });
    filesDB.save();
    return arrOfFiles.map((file)=>{return file.name});
  },
  'clientFilesHTTPDelete'(clientId, acc_token, flLstJSON){
    const updClt = client.findOne({access_token:acc_token, id:clientId});
    const clFiles = fileFuncsCheck(updClt, flLstJSON, "clientFilesHTTPDelete");
    let filesDB = fileList.findOne({client_id:clientId});
    const arrOfFiles = Array.isArray(clFiles) ? clFiles : [clFiles];
    if(!filesDB || !filesDB.files || filesDB.files.length == 0)
      makeError('clientFilesHTTPDelete.notFound', "Nothing to delete", 404);
    arrOfFiles.forEach((httpFile)=>{
      if(!httpFile.name || !httpFile.type)
        makeError('clientFilesHTTPDelete.empty', "No data provided", 204);
      let index = filesDB.files.findIndex((dbFile) => {return (httpFile.name === dbFile.name) && (httpFile.type === dbFile.type)});
      if(index == -1)
        makeError('clientFilesHTTPDelete.noFile',`File ${httpFile.name} doesn't exist`,404);
      filesDB.files.splice(index,1);
    });
    filesDB.validate(function(err) {
      if(err)
      {
        if (ValidationError.is(err)) {
          makeError('clientFilesHTTPDelete.inappropData', "Data passed couldn't be validated", 404);
        }
        makeError(`clientFilesHTTPDelete.${err.error}`, "An error occured. Try requesting later", 404);
      }
    });
    filesDB.save();
    return arrOfFiles.map((file)=>{return file.name});
  }
});

Meteor.startup(() => {

});
