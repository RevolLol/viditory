import { Meteor } from 'meteor/meteor';
export function timePassed(since=0, precisely=false)
{
  let time = (new Date().getTime() - since)/1000;
  return precisely ? time : Math.round(time);
}

export function makeError(name="Meteor Error", desc="Server error", statusCode)
{
  let error = new Meteor.Error(name,desc);
  if(statusCode)
    error.statusCode = statusCode;
  throw error
}

export function fileFuncsCheck(updClt, data, methodName)
{
  if(!updClt)
    makeError(`${methodName}.notFound`, "Couldn't find a client with specified params", 404);
  if(updClt.expires_at < new Date().getTime())
    makeError(`${methodName}.tokenExpired`, "Access token has expired", 401);
  try {
    let parsedData = JSON.parse(data, (key, value)=>{
      if(key == "fileIcon")
      {
        try {
          //checking if base64 icon string is ok
          return btoa(atob(value)) == value;
        } catch (e) {
          //skipping invalid base64 icon
          return undefined
        }
      }
      //if type is not one of the following, gonna presume it's file
      if(key == "type")
        return (value === "file" || value === "directory") ? value : 'file'
      //other keys dont require additional actions, so simply return the value
      return value
    });
    if( (Array.isArray(parsedData) && parsedData.length == 0) || (typeof parsedData === 'object' &&  Object.keys(parsedData).length === 0) )
    {
      throw new ReferenceError
    }
    return parsedData
  } catch (e) {
    if(e instanceof SyntaxError)
    {
      //JSON parse error
      makeError(`${methodName}.JSONparse`,"Error occured while parsing JSON-stringified object", 403);
    }
    if(e instanceof ReferenceError)
      makeError(`${methodName}.empty`, "No data provided", 204);
  }
}

export function directGoogleLinkMaker(link)
{
  const directLinkPart = "https://drive.google.com/uc?export=download&id=";
  //different types of links
  const id = link.indexOf("?id="), sharing = link.indexOf("/d/");
  //~n is equivalent to -(n+1)
  if(~id)
  {
    return `${directLinkPart}${link.slice(id+4)}`
  }
  if(~sharing)
  {
    return `${directLinkPart}${link.slice(sharing+3,link.lastIndexOf("/"))}`
  }
  //no matching pattern found
  return link
}

export function localStorageSupport() {
  try {
    return 'localStorage' in window && window['localStorage'] !== null;
} catch (e) {
    return false;
  }
}
