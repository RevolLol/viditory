import { Meteor } from 'meteor/meteor';
import { client } from '../api/collections.js';
import { createContainer } from 'meteor/react-meteor-data';
import clientUI from '../ui/clientUI.js';

/*container for list of client's files*/
export default clientContainer = createContainer(({ params }) => {
  const { client_id, dropOpt } = params;
  const clientHandle = Meteor.subscribe('clients.one', client_id);
  const loading = !clientHandle.ready();
  const clnt = client.findOne({id:client_id});
  const clientExists = !loading && !!clnt;
  return {
    loading,
    clnt,
    clientExists,
    client:clientExists ? clnt : {},
    client_id,
    dropOpt //binded methods for deleting and activating a client.
  };
}, clientUI);
