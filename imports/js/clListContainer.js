import { Meteor } from 'meteor/meteor';
import { client } from '../api/collections.js';
import { createContainer } from 'meteor/react-meteor-data';
import clListUI from '../ui/clListUI.js';

/*container for list of client's files*/
export default clListContainer = createContainer(({ params }) => {
  const { dlg } = params;
  const clientsHandle = Meteor.subscribe('clients.all');
  const loading = !clientsHandle.ready();
  const clList = client.find({userId:Meteor.userId()});
  const listExists = !loading && !!clList;
  return {
    loading,
    listExists,
    cls: listExists ? clList.fetch() : [],
    dlg
  }
}, clListUI);
