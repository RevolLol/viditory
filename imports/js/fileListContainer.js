import { Meteor } from 'meteor/meteor';
import { fileList } from '../api/collections.js';
import { createContainer } from 'meteor/react-meteor-data';
import fileListUI from '../ui/fileListUI.js';

/*container for list of client's files*/
export default fileListContainer = createContainer(({ params }) => {
  const { client_id } = params;
  const filesHandle = Meteor.subscribe('fileList.one', client_id);
  const loading = !filesHandle.ready();
  const flist = fileList.findOne({client_id},{fields:{files:1, filesLog:1, pathToDir:1}});
  const listExists = !loading && !!flist;
  return {
    loading,
    listExists,
    flList:listExists ? flist : {}
  }
}, fileListUI);
