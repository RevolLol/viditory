import React from 'react';
import ReactDOM from 'react-dom';
import signInBtn from "./signInBtnUI.js";
const rcE = React.createElement;//shorthand

export default class signInCard extends React.Component
{
	render()
	{
		return rcE("div",{id:"signInCard"},
      rcE("div",{key:"signInCard general-info", style:{"padding":"2vh"}},
        rcE("p",null,"Seems like you're not authorized or registered yet. Click the button below to get started"),
        rcE(signInBtn,{stateChange:this.props.stateChange})
      ),
      rcE("button",{key:"signInCard expand-button", "data-toggle":"collapse", "data-target":"#expand-info", className:"btn btn-default expand-button"},"Why use Google Account?"),
      rcE("div",{id:"expand-info", className:"collapse"}, "Google authorization is used in order to allow our client applications to upload and download file to user's Google Drive. Your private information is absolutely safe within our databases and will not be exposed or distributed to any other third-party services.")
		)
	}
}
