import { HTTP } from 'meteor/http';
import React from 'react';
import ReactDOM from 'react-dom';
import fileListContainer from "../js/fileListContainer.js"
import dropDown from "./dropDownUI.js"
const rcE = React.createElement;//shorthand

export default class clientUI extends React.Component
{

	constructor(props)
	{
		super(props);
		this.state = {active:props.client.active || false};
	}

	componentWillReceiveProps(nextProps)
	{
		if(this.state.active !== nextProps.client.active)
			this.setState({active:nextProps.client.active});
	}

	render()
	{
		/*user needs to select a client first*/
		/*if(!this.props.client_id)
			return null;//it's possible to return an element with a hint to user of further actions  he needs to take
		if(this.props.loading)
		{
			return rcE("div",{className:"clientContainer"}, rcE("div",{className:"clientInfo"}, "Loading"))
		}*/
		const nameLine = rcE("div",null,
			rcE("h4",null,this.props.client.name || "Name's not specified"),
			rcE(dropDown,{options:this.props.dropOpt})
		);
		return rcE("div",{className:"clientContainer"},
			rcE("div",{className:"clientInfo"},
				nameLine,
				/*passing client_id down to fileListContainer in order to get required file list*/
				this.state.active ? rcE(fileListContainer,{params:{client_id:this.props.client.id}}) : rcE("p",{className:"fileContainerWrapper"},"This client is deactivated. You can reactivate it in the menu above")
			)
		)
	}
}
