import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import {dialog} from "./dialogUI.js";
//import signInBtn from "./signInBtnUI.js";
import signInCard from "./signInCardUI.js";
import clListContainer from "../js/clListContainer.js";
import { ThreeColumnComps } from "/imports/api/helperObjs.js";
//import MobileDetect from 'mobile-detect';
const rcE = React.createElement;//shorthand

class header extends React.Component {
	render() {
		//checking if it collapsing button and collapsing div are required
		const noElems = this.props.nav.length == 0 && this.props.textElems.length == 0 && this.props.buttons.length == 0;
		return rcE("nav", {className:"navbar navbar-default navbar-fixed-top"},
			rcE("div",{className:"container-fluid"},
				rcE("div",{className:"navbar-header navbar-left"},
					noElems ? "" : rcE("button", {"type":"button", className:"navbar-toggle", "data-toggle":"collapse", "data-target":"#collapseNav"},
						rcE("span",{className:"icon-bar"}),
						rcE("span",{className:"icon-bar"}),
						rcE("span",{className:"icon-bar"})
					),
					rcE("div",{className:"navbar-brand"},"Viditory")
				),
				rcE("p",{className:"navbar-text navbar-left"},"A service that lets you keep your cloud storage managment easy"),
				noElems ? "" : rcE("div",{className:"collapse navbar-collapse", id:"collapseNav"},
					rcE("ul",{className:"nav navbar-nav"},
						this.props.nav.map((elem)=>{
							return rcE("li",null,elem)
						})
					),
					this.props.textElems.map( (text)=>{
						return rcE("p",{className:"navbar-text navbar-right"},text)
					}),
					(this.props.buttons || "")
				)
			)
		)
	}
}

class main extends React.Component
{
	render()
	{
		let center = this.props.center || "Sample main text";
		return rcE("div",{id:"main-wrapper", className:"container", key:"main-wrapper"},
	 		rcE("div",{id:"main", className:"row"},
				rcE("div",{className:`col-md-2`,key:"mLSide"},this.props.lside),
				rcE("div",{className:`col-xs-12 col-md-8`,key:"mCenter"}, center),
				rcE("div",{className:`col-md-2`,key:"mRSide"},this.props.rside)
			)
		)
	}
}

class footer extends React.Component {
	render() {
		const navHeader = this.props.header || "Sample footer text";
		return rcE("nav", {className:"navbar navbar-default navbar-fixed-bottom"},
			rcE("div",{className:"container-fluid"},
				rcE("div",{className:"navbar-header"},rcE("div",{className:"navbar-brand", style:{"disply":"inline-block"}},navHeader)),
				rcE("ul",{className:"nav navbar-nav"},this.props.nav)
			)
		)
	}
}

/*add type check*/
export default class appUI extends React.Component
{
	constructor(props)
	{
		super(props);
		this.state = {ShowDlg:props.ShowDlg || false, ShowSplash:props.ShowSplash || false, ShowAuth:props.ShowAuth || false, dlgBtns:props.dlgBtns || [], dlgInfo:props.dlgInfo || {}};
		//binding ensures that those methods wont loose their context when being passed somewhere else
		this.show_dialog = this.show_dialog.bind(this);
		this.show_auth = this.show_auth.bind(this);
	}

	show_dialog(state, info, buttons)
	{
		let defClose = rcE("button",{key:"dialog-hide", "data-dismiss":"modal", className:"btn btn-success btn-block"},"Ok");
		let dlgBtns = (state && !buttons) ? defClose : buttons;
		this.setState({ShowDlg:state, dlgBtns, dlgInfo: info});
	}

	show_splash(bool)
	{
		this.setState({ShowSplash:bool});
	}

	show_auth(bool)
	{
		this.setState({ShowAuth:bool});
	}

	createNewClient()
	{
		Meteor.call('clients.addNew', (err, res) => {
		  if (err) {
				this.show_dialog(
					true,
					{title:"Notification", text:`${err}`}
				);
		  } else {
				this.show_dialog(
					true,
					{title:"Notification", text:"Client was successfully added. Activate it in the 3-dot client menu in order to use it"}
				);
		  }
		});
	}

	render()
	{
		//let md = new MobileDetect(window.navigator.userAgent);

		//plain and simple copies of a template object
		let headerComps = {nav:[], textElems:[], buttons:[]},
		mainComps = JSON.parse(JSON.stringify(ThreeColumnComps)),
		footerComps = {header, nav:[]};
		footerComps.header = "Viditory Team 2016-2017";
		//mainComps.center.push(rcE("h3",null,"A service that lets you keep your cloud storage managment easy"));
		if(this.state.ShowAuth)//user is anauthorized
		{
			mainComps.center.push(rcE(signInCard,{stateChange:this.show_auth}));
		}
		else {
			headerComps.buttons.push(rcE("button",{
				key:"sign-out",
				className:"btn btn-warning navbar-btn navbar-right",
				id:"logout",
				onClick:()=>{
					this.show_dialog(
						true,
						{title:"Logging out", text:"You're about to log out. Are you sure you want to do this?"},
						[
							rcE("button",{key:"dialog-confirm", type:"button", className:"btn btn-success", "data-dismiss":"modal", onClick:()=>{Meteor.logout(this.show_auth.bind(this,!this.state.ShowAuth))}},"Confirm"),
							rcE("button",{key:"dialog-hide", type:"button", className:"btn btn-danger", "data-dismiss":"modal"},"Cancel")
						]
					);
				}
			},"Logout"));
			headerComps.textElems.push("Logged in as " + (Meteor.user().username ? Meteor.user().username : `User_${Meteor.user()._id}`));
			mainComps.center.push(
				rcE("button",{key:"client-gen", className:"btn btn-default", onClick:()=>{this.createNewClient()}},"Create new client"),
				rcE(clListContainer,{key:"clList", params:{dlg:(state, info, buttons)=>{this.show_dialog(state, info, buttons)}}})
			);
		}
		return rcE("div",null,
			this.state.ShowDlg ? rcE(dialog,{buttons:this.state.dlgBtns, info:this.state.dlgInfo, hideCallback:()=>{this.show_dialog(false)}}) : null,
			rcE(header,headerComps),
			rcE(main,mainComps),
			rcE(footer, footerComps)
		);
	}
}
