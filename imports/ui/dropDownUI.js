import React from 'react';
import ReactDOM from 'react-dom';
const rcE = React.createElement;//shorthand

export default class dropDown extends React.Component
{
	render()
	{
		if(!this.props.options)
		{
			return null;
		}
		return rcE("div",{className:"dropdown", key:`dropdown-menu-${this.props.options.length}`},
      rcE("button", {className:"btn btn-primary dropdown-toggle", type:"button", "data-toggle":"dropdown"},String.fromCodePoint(0x22EE)),
      rcE("ul",{className:"dropdown-menu dropdown-menu-right"},
        this.props.options.map((option, number)=>{
          return rcE("li",{key:`dropdown-option-${number}`},option)
        })
      )
		)
	}
}
