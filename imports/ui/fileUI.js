/*TODO: handle possible contens of the directory
*/

import React from 'react';
import ReactDOM from 'react-dom';
const rcE = React.createElement;//shorthand

// File component representing one syncing file
export default class fileUI extends React.Component {
  render() {
    /*status can either be an upload percentage or a state like done or error*/
    const stat = isNaN(parseInt(this.props.stat)) ? this.props.stat : `${this.props.stat}% done`;
    /*if there's a bitmap icon in the database, we're gonna get it like this. otherwise a default file type icon is gonna be used*/
    const icon = this.props.fileIcon ? 'data:image/bmp;base64,' + btoa(this.props.fileIcon): `/res/icons/${this.props.type}.png`;
    const haveContents = this.props.type === "directory" && this.props.contents && this.props.contents.length != 0;
    return rcE("li",{"data-toggle":"collapse", "data-target":`#${this.props.name}-${this.props.type}-contents`, key:`singleFile-${this.props.name}-${this.props.type}`},
      rcE("div",{className:"singleFile", style:{"cursor":haveContents ? "pointer": "default"}},
        rcE("img",{className:"fileIcon", key:`fileIcon-${this.props.name}-${this.props.type}`, src:icon}),
        rcE("span",{className:"fileName", key:`fileName-${this.props.name}-${this.props.type}`},this.props.name),
        rcE("span",{className:"fileStat", key:`fileStat-${this.props.name}-${this.props.type}`},stat)
      ),
      haveContents ? rcE("ul", {id:`${this.props.name}-${this.props.type}-contents`, className:"collapse"}, this.props.contents.map( (item)=>{return rcE("li",null,item)} )) : ""
    )
  }
}
