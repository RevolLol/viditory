import React from 'react';
import ReactDOM from 'react-dom';
import fileUI from "./fileUI.js"
const rcE = React.createElement;//shorthand

//component represents a list of syncing files for 1 client
export default class fileListUI extends React.Component {

  constructor(props)
  {
    super(props);
    this.state = {showLog:false};
  }

  render() {
    /* because Object.keys(new Date()).length === 0;
    we have to do some additional check*/
    if(this.props.loading)
      return rcE("div",{className:"fileContainer"},"Loading...")
    const addInfo = this.state.showLog ? rcE("p",null,
      "List of previous files",
      rcE("button",{className:"btn btn-default",onClick:()=>{this.setState({showLog:false})}},"Show current files")
    ) : rcE("p",null,
      `Application's working directory: ${this.props.flList.pathToDir || "Path was not specified"}`,
      rcE("button",{className:"btn btn-default", onClick:()=>{this.setState({showLog:true})}},"Show log files")
    );
    if(!this.state.showLog && (Object.keys(this.props.flList).length === 0 || this.props.flList.files.length === 0))
      return rcE("div",{className:"fileContainerWrapper"},
        addInfo,
        rcE("p",{className:"fileContainer"},"No files were found")
      )
    if(this.state.showLog && (Object.keys(this.props.flList).length === 0 || this.props.flList.filesLog.length === 0))
      return rcE("div",{className:"fileContainerWrapper"},
        addInfo,
        rcE("p",{className:"fileContainer"},"No files were found in log")
      )
    return rcE("div",{className:"fileContainerWrapper"},
      addInfo,
      rcE("ul",{className:"fileContainer"}, this.state.showLog ? this.props.flList.filesLog.map((item)=>{return rcE(fileUI,item)}) : this.props.flList.files.map((item)=>{return rcE(fileUI,item)}))
    )
  }
}
