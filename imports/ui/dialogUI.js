import React from 'react';
import ReactDOM from 'react-dom';
const rcE = React.createElement;//shorthand

/*implement states and button options*/
export class dialog extends React.Component
{

	componentDidMount(){
			$(ReactDOM.findDOMNode(this)).modal();//show dialog as soon as it was mounted
			$(ReactDOM.findDOMNode(this)).on('hidden.bs.modal', this.props.hideCallback);//a callback is being invoked every single time dialog hides in order to remove it from DOM in AppUI
	}

	render()
	{
		if(!this.props.buttons)
		{
			return null;
		}
		let {title, text} = this.props.info ? this.props.info: {title:"Notification", text:"Default text"};
		return rcE("div",{className:"modal fade", role:"dialog"},
			rcE("div",{className:"modal-dialog"},
				rcE("div",{className:"modal-content"},
					rcE("div",{className:"modal-header"},
						rcE("h4",{className:"modal-title"},title)
					),
					rcE("div",{className:"modal-body"},text),
					rcE("div",{className:"modal-footer"},this.props.buttons)
				)
			)
		)
	}
}
