import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
const rcE = React.createElement;//shorthand

export default class SignInBtn extends React.Component
{

	constructor(props)
	{
		super(props);
		this.login = this.login.bind(this);
		this.state = {loginReady:Accounts.loginServicesConfigured()};
	}

	login(stateChangeFunc)
	{
		/*requesting offline access to get a refresh token*/
		Meteor.loginWithGoogle({requestPermissions:["openid profile email https://www.googleapis.com/auth/drive"], requestOfflineToken:true, loginStyle:"popup", prompt:"select_account"}, (err) => {
			if (err) {
				alert("Something went wrong");
			} else {
				stateChangeFunc(false);
			}
		});
	}

	render()
	{
		return rcE("div",{id:"gSignInWrapper"},
			rcE("span",{className:"label"},"Sign in with:"),
			rcE("p",{id:"customBtn",className:"customGPlusSignIn", onClick:()=>{this.login(this.props.stateChange)}},
				rcE("span",{className:"icon"}),
				rcE("span",{className:"buttonText"},"Google")
			)
		)
	}
}
