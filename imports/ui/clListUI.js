import React from 'react';
import ReactDOM from 'react-dom';
//import clientContainer from "../js/clientContainer.js";
import dropDown from "./dropDownUI.js";
import { directGoogleLinkMaker } from "/imports/api/helperFuncs.js";
import { localStorageSupport } from "/imports/api/helperFuncs.js";
import clientUI from '../ui/clientUI.js';
const rcE = React.createElement;//shorthand

// File component representing one syncing file
export default class clListUI extends React.Component {

  constructor(props) {
    super(props);
    //last viewed client
    const lastClient = localStorageSupport() ? Number(localStorage.getItem("lastClient")) : 0;
    this.state = {value: lastClient || 0};
    this.handleListChange = this.handleListChange.bind(this);
    this.changeActState = this.changeActState.bind(this);
    this.deleteClient = this.deleteClient.bind(this);
  }

  componentDidMount()
  {
    /*thingy for copying client data to clipboard*/
    const clipboard = new Clipboard('#clipbrd');
    clipboard.on('success',(e)=>{
      this.props.dlg(
        true,
        {title:"Notification", text:"Client's data was copied to your clipboard. You will be asked to provide this data on the first application launch"}
      );
    });
  }

  componentDidUpdate(prevProps, prevState)
  {
    if((this.state.value != prevState.value) && localStorageSupport())
    {
      localStorage.setItem("lastClient", this.state.value);
    }
  }

  componentWillReceiveProps(nextProps)
	{
    /*there's a requirement to decrease current viewed client's number in case user've just deleted the last client*/
    if(this.props.listExists)
    {
      if(this.props.cls.length > nextProps.cls.length && this.state.value)
        this.setState({value:this.state.value-1});
      if(this.props.cls.length < nextProps.cls.length)
        this.setState({value:nextProps.cls.length-1});
    }
	}

  handleListChange(event) {
    this.setState({value: event.target.value});
  }

  changeActState()
	{
    const state = !this.props.cls[this.state.value].active;
		Meteor.call('clients.changeActState',{client_id:this.props.cls[this.state.value].id, newState:state} ,(err, res) => {
		  if (err) {
        this.props.dlg(
					true,
					{title:"Notification", text:`Error "${err.message}" occured`}
				);
		  } else {
				const stateMessage = state ? "activated. Copy client's data by clicking 'Copy data to clipboard' in client's 3-dot menu." : "deactivated. To continue using it you need to activate it in client's 3-dot menu.";
				this.props.dlg(
					true,
					{title:"Notification", text:`Client was successfully ${stateMessage}`}
				);
		  }
		});
	}

	deleteClient()
	{
		Meteor.call('clients.deleteClient', this.props.cls[this.state.value].id, (err, res) => {
			if (err) {
        this.props.dlg(
					true,
					{title:"Notification", text:`Error "${err.message}" occured`}
				);
			} else {
        this.props.dlg(
          true,
          {title:"Notification", text:`Client was successfully deleted`}
        );
			}
		});
	}

  downloadClient()
  {
    Meteor.call('makeClientZip',this.props.cls[this.state.value].id, (err, res) => {
      if (err) {
        this.props.dlg(
          true,
          {title:"Notification", text:`Error "${err.message}" occured`}
        );
      } else {
        this.props.dlg(
          true,
          {title:"Notification", text:`Client was successfully generated`}
        );
      }
    });
  }

  clearLog()
  {
    Meteor.call('clients.clearLog',this.props.cls[this.state.value].id, (err, res) => {
      if (err) {
        this.props.dlg(
          true,
          {title:"Notification", text:`Error "${err.message}" occured`}
        );
      } else {
        this.props.dlg(
          true,
          {title:"Notification", text:`Client's log was successfully cleared`}
        );
      }
    });
  }

  render() {
    /*data's still loading*/
    if(this.props.loading)
      return rcE("div",{id:"clList", className:"row", key:"clList"},
        rcE("div",{className:"col-md-12"},"Loading...")
    )

    /*user has no clients yet*/
    if(!this.props.listExists || !this.props.cls.length)
    {
        return rcE("h3",{className:"clList", key:"clList"},"You've got no clients yet. Hit the 'Create new client' button to get started")
    }

    /*a fallback that allows user to pick a client from a list in case they break a default client selector somehow*/
    if(this.state.value < 0 || this.state.value > this.props.cls.length-1)
    {
      let clArr = this.props.cls.map((client,number)=>{return rcE("option",{key:client.id, value:number},client.name)});
      clArr.push(rcE("option",{key:"defaultOption", value:"-1", disabled:true, selected:true, hidden:true}, "Select a client..."));
      return rcE("div",{id:"clList", key:"clList"},
        rcE("div",null,"Something went terribly wrong and this client doesn't exist. You can pick an existing one from a list below",
          rcE("p",null,rcE("select",{key:"clListSel", onChange:this.handleListChange},clArr))
        )
      )
    }
    return rcE("div",{key:"clList", className:"clList row"},
      rcE("button",{
        key:"clListLSide",
        onClick:()=>{if(this.state.value > 0) {this.setState({value:this.state.value-1})}},
        className:"clientSwitchBtn btn btn-default",
        disabled:!(this.state.value > 0)}, "<"
      ),
      rcE(clientUI, {client:this.props.cls[this.state.value],dropOpt: this.props.cls[this.state.value].active ? [
          rcE("a",{href:"#", onClick:()=>{this.changeActState()}}, "Deactivate client"),
          rcE("a",{href:"#", onClick:()=>{this.deleteClient()}}, "Delete client"),
          rcE("a",{href:directGoogleLinkMaker(Meteor.settings.public.download) || "#", target:"_blank"}, "Download client"),
          rcE("a",{href:"#", id:"clipbrd", "data-clipboard-text":this.props.cls[this.state.value].encrData}, "Copy data to cliboard"),
          rcE("a",{href:"#", onClick:()=>{this.clearLog()}}, "Clear client's log")
        ] : [
          rcE("a",{href:"#", onClick:()=>{this.changeActState()}}, "Activate client"),
          rcE("a",{href:"#", onClick:()=>{this.deleteClient()}}, "Delete client")
        ]}),
      /*rcE(clientContainer, {params:
          {
            client_id:this.props.cls[this.state.value].id || 0,
            dropOpt:[
              rcE("a",{href:"#", onClick:()=>{this.changeActState()}}, `${this.props.cls[this.state.value].active ? "Deactivate": "Activate"} client`),
              rcE("a",{href:"#", onClick:()=>{this.deleteClient()}}, "Delete client"),
              //rcE("a",{href:"#", onClick:()=>{this.downloadClient()}}, "Generate zip"),
              rcE("a",{href:directGoogleLinkMaker(Meteor.settings.public.download) || "#", target:"_blank"}, "Download client"),
              rcE("a",{href:"#", id:"clipbrd", "data-clipboard-text":this.props.cls[this.state.value].encrData}, "Copy data to cliboard"),
              rcE("a",{href:"#", onClick:()=>{Meteor.call('clientsFiles.testAddFilest',this.props.cls[this.state.value].id)}}, "[Test] Add files")
            ]
          }
      }),*/
      rcE("button",{
        key:"clListRSide",
        onClick:()=>{if(this.state.value < this.props.cls.length-1) {this.setState({value:this.state.value+1})}},
        className:"clientSwitchBtn btn btn-default",
        disabled:!(this.state.value < this.props.cls.length-1)}, ">"
      )
    )
  }
}
